package dev.emortal.marathon

import net.minestom.server.instance.block.Block

enum class BlockPalette(vararg val blocks: Block) {
    DEFAULT(Block.GRASS_BLOCK, Block.OAK_LOG, Block.BIRCH_LOG, Block.OAK_LEAVES, Block.BIRCH_LEAVES)
}